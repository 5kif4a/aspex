import os
from os.path import join, dirname
from dotenv import load_dotenv

dotenv = join(dirname(__file__), '.env')
load_dotenv(dotenv)

USER_LOGIN = os.environ.get('USER_LOGIN')
USER_HASH = os.environ.get('USER_HASH')
SUBDOMAIN = os.environ.get('SUBDOMAIN')
