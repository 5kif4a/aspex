**amoCRM API**

Требования: Python 3.6+

**Установка**
- Скачать проект
- В директории проекта установить виртуальное окружение
```
cd path_to_project
pip install virtualenv
py -m venv venv
```
- Активировать виртуальную среду
```
# cmd.exe
venv\Scripts\activate.bat
```
- Установить требуемые зависимости
```
# cmd.exe
pip install -r requirements.txt
```
- Создать и настроить файл .env в корне проекта конфигурации переменных окружения перед запуском скрипта
```
USER_LOGIN=login@email.com
USER_HASH=user_hash_from_amoCRM
SUBDOMAIN=subdomain
```
- Запустить скрипт
```
py main.py
```
Скачанные данные будут находиться в файле leads.json в директории output


