import requests as r
import json
from pathlib import Path
from settings import USER_LOGIN, USER_HASH, SUBDOMAIN

auth_url = f'https://{SUBDOMAIN}.amocrm.ru/private/api/auth.php'
leads_url = f'https://{SUBDOMAIN}.amocrm.ru/api/v2/leads'

session = r.Session()
auth_params = {
            'USER_LOGIN': USER_LOGIN,
            'USER_HASH': USER_HASH,
            'type': 'json'
        }
session.post(auth_url, params=auth_params)
cookies = session.cookies


def get_all_leads(limit_offset, limit_rows):
    result = []
    params = {
        'limit_rows': limit_rows,
        'limit_offset': limit_offset
    }
    s = session.get(leads_url, cookies=cookies, params=params)
    if s.status_code != 204:
        try:
            result.append(s.json()['_embedded']['items'])
        except:
            pass
        limit_offset += limit_rows
        result += get_all_leads(limit_offset, limit_rows)
    return result


items = sum(get_all_leads(0, 500), [])  # flat list
data = dict(items=items)


p = Path('output/')
p.mkdir(parents=True, exist_ok=True)
outpath = Path.cwd() / 'output' / 'all_leads.json'
with outpath.open('w', encoding='utf8') as f:
    f.write(json.dumps(data, indent=4, ensure_ascii=False))
