import requests as r
import pendulum as pnldm
import time
from classes.sessionwithbaseurl import SessionWithBaseUrl
from classes.logger import logger


class API:
    """
    Class for requesting data using amoCRM API
    """

    def __init__(self, user_login, user_hash, subdomain):
        self.__user_login = user_login
        self.__user_hash = user_hash
        self.__subdomain = subdomain
        self.__authorized = False
        self.__last_error = 'No errors were observed'
        self.__base_url = f'https://{self.__subdomain}.amocrm.ru/api/v2/'

        self.__auth()

    def __str__(self) -> str:
        return f'USER_LOGIN: {self.__user_login}, SUBDOMAIN: {self.__subdomain}, AUTHORIZED: {self.__authorized}'

    def get_auth_status(self) -> bool:
        """
        get authorization status
        :return: True|False
        """
        return self.__authorized

    def get_error_message(self) -> str:
        """
        get last error message
        :return: str
        """
        return self.__last_error

    def __request(self, url: str, method: str, **kwargs) -> r.Response:
        """
        private request method
        :param url: url for request
        :param method: HTTP method type (get, post, etc)
        :param kwargs: keywords args
        :return: Response
        """
        session = SessionWithBaseUrl(self.__base_url)
        try:
            res = session.request(method, url, **kwargs)
            return res
        except r.RequestException as ex:
            self.__last_error = str(ex)
            logger.error(ex)

    def __make_auth(self):
        """
        Authorization request
        :return: request
        """
        params = {
            'USER_LOGIN': self.__user_login,
            'USER_HASH': self.__user_hash,
            'type': 'json'
        }
        req = self.__request('/private/api/auth.php', 'post', params=params)
        return req

    def __repeat_auth(self):
        """
        When entering incorrect authentication data
        reauthorization with manual data entry
        :return: int - response status code
        """
        self.__subdomain = input('SUBDOMAIN: ')
        self.__user_login = input('USER_LOGIN: ')
        self.__user_hash = input('USER_HASH: ')
        self.__base_url = f'https://{self.__subdomain}.amocrm.ru/api/v2/'
        return self.__make_auth()

    def __auth(self) -> int:
        """
        Authorization method
        :return: int - response status code
        """

        # первичная авторизация
        first_auth = self.__make_auth()

        # проверка первичной авторизации
        if first_auth.json().get('response', {}).get('auth', False) is True:
            self.__authorized = True
            self.__cookies = first_auth.cookies
            logger.info(self.__str__())
            return first_auth.status_code
        else:
            # вторичная авторизация
            print('Ошибка авторизации. Введите авторизационные данные')
            second_auth = self.__repeat_auth()

            # проверка вторичной авторизации
            if second_auth.json().get('response', {}).get('auth', False) is True:
                self.__authorized = True
                self.__cookies = second_auth.cookies
                logger.info(self.__str__())
                return second_auth.status_code
            else:
                self.__last_error = 'Ошибка авторизации. Аккаунт не существует или введен неверный пароль или логин'
                logger.error('Ошибка авторизации. Аккаунт не существует или введен неверный пароль или логин')
                return 401

    def get_leads_by_status_and_period(self, statuses: tuple, period: tuple) -> dict:
        """
        get account's data about leads filtered by status and period(from, to)
        :param statuses: status - tuple of code(int or str)
        :param period: tuple of dates in string format YYYY/MM/DD
        :return: dict - json response
        """
        if self.__authorized:
            from_ = pnldm.from_format(period[0], 'YYYY/MM/DD').int_timestamp
            to_ = pnldm.from_format(period[1], 'YYYY/MM/DD').int_timestamp
            params = {
                    'status[]': statuses,
                    'filter[date_create][from]': from_,
                    'filter[date_create][to]': to_
            }
            resp = self.__request('leads', 'get', params=params, cookies=self.__cookies)
            return resp.json()
        else:
            self.__auth()

    def get_contacts_by_id(self, id_list: list) -> dict:
        """
        get account's data about contacts
        :param: id_list: list of id
        :return: dict - json response
        """
        if self.__authorized:
            params = {
                'id[]': id_list
            }
            resp = self.__request('contacts', 'get', params=params, cookies=self.__cookies)
            return resp.json()
        else:
            self.__auth()

    def get_all_events_by_period(self, period: tuple) -> list:
        """
        getting all events for a certain period
        :param period: tuple of dates in string format DD.MM.YYYY
        :return: list with all events(logs)
        """
        if self.__authorized:
            events = []
            params = {
                'filter_date_from': period[0],
                'filter_date_to': period[1],
            }
            headers = {'x-requested-with': 'XMLHttpRequest'}

            # get total page count
            resp = self.__request('/ajax/events/count', 'get', params=params, cookies=self.__cookies, headers=headers)
            pages = resp.json().get('pagination', {}).get('total', 0)
            print(f'Total page count: {pages}')

            # get all events
            for page in range(1, pages + 1):
                print(f'Sampling data from page {page}...')
                params.update({'PAGEN_1': page})
                resp = self.__request('/ajax/events/list', 'get', params=params, cookies=self.__cookies,
                                      headers=headers)
                events_for_page = resp.json().get('response', {}).get('items', [])
                events.append(events_for_page)
                # every 4 iteration(request) - delay 1 second
                if page % 4 == 0:
                    time.sleep(1)

            return sum(events, [])  # flat list, list with each event, not a list with events for each page
        else:
            self.__auth()


if __name__ == '__main__':
    USER_LOGIN = 'Service@aspex.kz'
    USER_HASH = '8875b2ad63b166a2ad9efe660831c252d9c90a7c'
    SUBDOMAIN = 'certitdev'
    api = API(USER_LOGIN, USER_HASH, SUBDOMAIN)
