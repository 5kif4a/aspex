import json
from pathlib import Path
from settings import USER_LOGIN, USER_HASH, SUBDOMAIN
from amocrm import API


def save_json(filename: str, directory: str, data: dict):
    p = Path(directory)
    p.mkdir(parents=True, exist_ok=True)
    outpath = Path.cwd() / directory / filename
    with outpath.open('w', encoding='utf8') as f:
        f.write(json.dumps(data, indent=4, ensure_ascii=False))


if __name__ == '__main__':
    api = API(USER_LOGIN, USER_HASH, SUBDOMAIN)
    period = ('01.09.2019', '30.09.2019')
    events = api.get_all_events_by_period(period)
    good_events = []
    bad_events = []

    # data integrity check
    for event in events:
        # tuple with bool values from checking the type of each required field
        type_checking = (type(event.get('value_before', False)) in (list, dict),
                         type(event.get('value_after', False)) in (list, dict),
                         type(event.get('object', False).get('id', False)) in (int, str),
                         type(event.get('object', False).get('entity', False)) in (int, str))

        # if at least one field does not match the type - bad event, else - good event
        if not all(type_checking):
            bad_events.append(event)
        else:
            good_events.append(event)

    # message info about events
    print(f'Events count: {len(events)}\nGood events count: {len(good_events)}\nBad events count: {len(bad_events)}')
    # saving data in files
    save_json('all_events.json', 'output/', dict(events=events))
    save_json('good_events.json', 'output/', dict(events=good_events))
    save_json('bad_events.json', 'output/', dict(events=bad_events))
