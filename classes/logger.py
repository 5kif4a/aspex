import logging

logger = logging.getLogger('amoCRM')
logger.setLevel(logging.INFO)
fh = logging.FileHandler('console.log')
formatter = logging.Formatter('%(name)s: [%(levelname)s] - %(asctime)s - %(message)s')
fh.setFormatter(formatter)
logger.addHandler(fh)
